FROM python:3.9-slim-buster

# Create app directory
RUN mkdir /app

# Copy all files to the app directory
COPY . /app

# Set the working directory to the app directory
WORKDIR /app

# Install dependencies
RUN apt-get update && apt-get install -y \
    gcc \
    g++ \
    make \
    libjpeg-dev \
    zlib1g-dev \
    libpng-dev \
    libfreetype6-dev \
    liblcms2-dev \
    libopenjp2-7-dev \
    libtiff-dev \
    tk-dev \
    tcl-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libssl-dev \
    libffi-dev \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

# Run the command within the app directory
CMD cd /app && streamlit run main.py --server.port 50004 --server.maxUploadSize 2000
